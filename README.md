# Docker-windows

This repository contains Docker installation instructions for Windows 10 laptops.<br>
Tested on Sogeti's Elitebook HSN-I24C-5.

## Downloading

Download Docker with WSL 2 support from the official [webpage](https://docs.docker.com/desktop/windows/install/). There
is also a tutorial available there.

_Direct download [link](https://desktop.docker.com/win/main/amd64/Docker%20Desktop%20Installer.exe)_

## Installing

### Install with WSL 2 enabled

When prompted, ensure the Install required Windows components for WSL 2 option is selected on the Configuration page.

### Continue and finish installation

No further instructions required. Just spam click next.

## Add User to docker-user

After the installation is completed, it is important to check if your user is added to the docker-user group.

### Run Computer management as administrator

Within Computer management, navigate to Local Users and Groups -> Groups -> docker-users.<br>
Right-click on docker-users to add your user to the group. Log out and log back in for the changes to take effect.

# Try to run Docker

If it works, great! If it doesn't continue with the following steps.

## Install and enable WSL 2

Official guide can be found [here](https://docs.microsoft.com/en-us/windows/wsl/install-manual)

### Open an elevated Powershell

Start -> type "powershell" -> right-click -> Run as administrator

### Enable WSL feature within Windows

Within the elevated Powershell, run the following command:<br>
`dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart`

### Enable Virtual Machine feature

Within the (same) elevated Powershell, run the following command:<br>
`dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart`

### Download and install Linux kernel update

Can be downloaded [here](https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi)

### Set WSL 2 as default version

Within the (same) elevated Powershell, run the following command: <br>
`wsl --set-default-version 2`