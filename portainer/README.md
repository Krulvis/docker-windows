# Installing portainer

Official guide can be found [here](https://docs.portainer.io/v/ce-2.9/start/install/server/docker/wsl)

## Create a new docker volume for Portainer

Run the following command in Powershell:<br>`docker volume create portainer_data`

## Download & Run a portainer container

Run the following command in Powershell:<br>
`docker run -d -p 8000:8000 -p 9443:9443 -p 9000:9000 --name portainer --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer-ce:latest`

I added the legacy port 9000 exposure parameter `-p 9000:9000` since for me the self-signed SSL certificate did not get
recognized by my laptop.

## Visit the portainer UI

You can find the Portainer UI [here](http://localhost:9000). Alternatively, try to access portainer over port 9443 to
check if the self-signed SSL certificate worked for you.

## Login and select environment

Create an account in the Portainer UI and select local as environment.

# Manage containers

You are now able to get an overview of all your stacks & containers by navigating to the Containers tab in the menu.