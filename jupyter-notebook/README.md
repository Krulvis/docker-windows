# Dockerized jupyter-notebook

In this readme I will be taking you through getting a dockerized jupyter notebook

## Base notebook (optional)

### Getting started

Getting started is really easy and with the following command you can get a basic jupyter-notebook container up and
running:<br>
`docker run -p 8888:8888 jupyter/scipy-notebook`
<br>
This command will download the image if its not already present on your computer and run a container using
the `jupyter/scipy-notebook` image.<br>

### Accessing the container

After the container is booted, the URL to access the container `http://127.0.0.1:8888/?token=<TOKEN_KEY>` is printed in
the output log. Copy and paste that URL into your browser to access the notebook.

### Password generation

Use the following code in the notebook to generate a password which we will use in the `Custom Container` instructions.
Change the `"password"` to whatever password you'd like to use for your containerized jupyter-notebook.

```python
import IPython

print(IPython.lib.passwd("password"))
```

This code prints the hash of a `sha1` hashed `"password"`. Copy the output and save it somewhere for later.

## Custom Container

Since we might want to re-use the same notebook throughout multiple sessions, we want to be able to save the work
progress. For this we want to add a volume. Likewise, we might have some python packages that we know we will require
for our work. For this we want to use a `requirements.txt` file in which we store the required pacakges.

### Kill the base notebook

If you still have the base notebook container running, go to your portainer page, navigate to containers and kill
the `jupyter/scipy-notebook` container.

### Dockerfile

To start of we will be creating a `Dockerfile` in which we compose the image to be used in the custom docker container.
<br>
Since we don't want to re-invent the wheel, we will be using a base image created by Jupyter: `base-notebook`. To do
this, the first important line in the Dockerfile is the following:<br>

```bash
FROM jupyter/base-notebook
```

<br>

Next, we will be copying over the `requirements.txt` file such that we can install any python package required for
whatever we will be doing on the docker image construction.

```bash
COPY requirements.txt /requirements.txt
```

<br>

Lastly, we will install all the packages listed in `requirements.txt` using the cross-platform mamba package manager.

```bash
RUN mamba install --file /requirements.txt
```

### Docker Compose

Now that we have a custom jupyter notebook image with all our custom python packages installed, it is time to construct
the container. This will be done in the `docker-compose.yml` file.

First, we declare the version and add the services (containers) that will be created from this compose file. Since we
only need one container, there is only one entry under services named `notebook`.
<br>
To make sure that docker-compose uses our custom `Dockerfile` we set the current directory to find the Dockerfile with
the following option:

```yaml
build: .
```

<br>
Since we will be accessing the docker container from our local machine we need to expose the port from within the docker
container. This is done by adding the following option in the notebook service:

```yaml
ports:
  - "8888:8888"
```

To get rid of requiring an access token every time we try to access the jupyter-notebook environment we add the password
generated previously by creating a custom `command`:

```yaml
command: "start-notebook.sh --NotebookApp.password=${ACCESS_TOKEN}"
```

The access token is retrieved from a `.env` file. This repository contains an `.env.example` as we should never push
actual passwords (or hashes) to a repository. <b>Rename the .env.example file to .env and replace the hash with your
generated hash</b>

<br>
To save the work that we do, we will be linking the `work` directory inside the container to a volume with the following option:

```yaml
volumes:
  - work:/work
```

To be able to use this volume we have to declare it outside of the services scope:

```yaml
volumes:
  work: { }
```


